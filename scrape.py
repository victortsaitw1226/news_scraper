#encoding: utf-8
from newspaper import Article
import codecs
import sys
import os
import urllib.request
import re
from shutil import copyfile

def scrape_news_by_url(url):
    a = Article(url, language='zh')
    a.download()
    a.parse()
    return a.title, a.text

def scrape_news_by_html(html):
    a = Article('', language='zh')
    a.set_html(html)
    a.parse()
    return a.title, a.text

def scrape_news_by_file(path):
    f = codecs.open(path, 'r', 'utf-8')
    return scrape_news_by_html(str(f.read()))

def download_html(url, folder):
    print('download:%s' % url)
    a = Article(url, language='zh')
    a.download()
    # news_path = url.split('.html')[0]
    news_path = url
    news_path = news_path.replace('\n','')
    news_path = news_path.replace(':','')
    news_path = news_path.replace('//','_')
    news_path = news_path.replace('/','_')
    path = os.path.join(os.getcwd(), folder, news_path)
    opener = urllib.request.build_opener()
    opener.addheaders=[('User-Agent','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1941.0 Safari/537.36')]
    urllib.request.install_opener(opener)
    urllib.request.urlretrieve(url, path)
    print(result)
    return news_path

def read_html_from_folder(folder):
    folder = os.path.join(os.getcwd(), folder)
    for filename in os.listdir(folder):
        yield os.path.join(os.getcwd(), folder, filename), filename

def check_pattern(content, keyword):
    c = re.compile(keyword)
    result = c.search(content)
    print('check_pattern:%s' % result)
    return result

def save_to_file(content, path):
    with open(path, 'w') as f:
        f.write(content)

#==================================
def download_from_config(url_config, raw_html_folder):
    with open(url_config, 'r') as f:
        url = f.readline()
        while url:
            print('download from %s' % url)
            try:
                download_html(url, raw_html_folder)
            except Exception as ex:
                url = f.readline()
                # continue
            url = f.readline()

def save_related_news(raw_html_folder):
     for html_file, filename in read_html_from_folder(raw_html_folder):
         title, contents = scrape_news_by_file(html_file)
         # print(title)
         # print(contents)
         if check_pattern(title, u'郭董') or \
         check_pattern(title, u'台銘') or \
         check_pattern(contents, u'郭董') or \
         check_pattern(contents, u'台銘'):
             path = os.path.join(os.getcwd(), 'related_news', 'all', filename)
             save_to_file(contents, path)

             if check_pattern(title, u'巴西') or check_pattern(contents, u'巴西'):
                 path = os.path.join(os.getcwd(), 'related_news', 'brazil', filename)
                 save_to_file(contents, path)
                 
             if check_pattern(title, u'印度') or check_pattern(contents, u'印度'):
                 path = os.path.join(os.getcwd(), 'related_news', 'india', filename)
                 save_to_file(contents, path)

             if check_pattern(title, u'印尼') or check_pattern(contents, u'印尼'):
                 path = os.path.join(os.getcwd(), 'related_news', 'indonesia', filename)
                 save_to_file(contents, path)

             if check_pattern(title, u'高雄') or check_pattern(contents, u'高雄'):
                 path = os.path.join(os.getcwd(), 'related_news', 'kaohsiung', filename)
                 save_to_file(contents, path)
             
             if check_pattern(title, u'賓州') or check_pattern(contents, u'賓州'):
                 path = os.path.join(os.getcwd(), 'related_news', 'pennsylvania', filename)
                 save_to_file(contents, path)

def main():
    raw_html_folder = 'download_news'
    url_config = sys.argv[1]
    print('url list:%s' % url_config)
    download_from_config(url_config, raw_html_folder)
    save_related_news(raw_html_folder)

if __name__ == '__main__':
    main()
